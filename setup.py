from setuptools import setup, find_packages

setup(
    name='gwb_launcher',
    version='1.0',
    author='QuarkyUp',
    packages=find_packages(exclude=["tests", "tests.*"]),
    license="",
    long_description=open("README.md").read(),
    include_package_data=True,
    classifiers={
        "Programming Language :: Python :: 3"
    },
    entry_points={
        "console_scripts": [
            "gwb_launcher = gwb_launcher.__main__:main",
        ],
    },
)
