import win32api
import win32con
import win32gui
import win32process


def get_gw_client_pids():
    def handker(hwnd, lParam):
        if "ArenaNet_Dx_Window_Class" == win32gui.GetClassName(hwnd):
            _, pid = win32process.GetWindowThreadProcessId(hwnd)
            lParam.append(pid)

    gw_window_pids = []
    win32gui.EnumWindows(handker, gw_window_pids)
    return gw_window_pids


def get_crashed_gw_client_pids():
    def handler(hwnd, lParam):
        if "#32770" == win32gui.GetClassName(hwnd):
            _, pid = win32process.GetWindowThreadProcessId(hwnd)
            lParam.append(pid)

    gw_window_pids = []
    win32gui.EnumWindows(handler, gw_window_pids)
    return gw_window_pids


def get_gw_window_handle(pid: int):
    data = {
        "pid": pid,
        "handle": None
    }

    def handler(hwnd, lParam):
        if "ArenaNet_Dx_Window_Class" == win32gui.GetClassName(hwnd):
            _, pid = win32process.GetWindowThreadProcessId(hwnd)
            if lParam["pid"] == pid:
                lParam["handle"] = hwnd

    win32gui.EnumWindows(handler, data)

    remote_thread, _ = win32process.GetWindowThreadProcessId(data["handle"])
    thread_id = win32api.GetCurrentThreadId()
    win32process.AttachThreadInput(thread_id, remote_thread, True)
    win32gui.SetFocus(data["handle"])

    win32gui.SendMessage(win32con.HWND_BROADCAST, win32con.WM_KEYDOWN, 0x50, 0x00190001)
    win32gui.SendMessage(win32con.HWND_BROADCAST, win32con.WM_CHAR, 'j', 0x00190001)
    win32gui.SendMessage(win32con.HWND_BROADCAST, win32con.WM_KEYUP, 0x50, 0x00190001)
