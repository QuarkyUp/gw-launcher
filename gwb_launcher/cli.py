from time import sleep

import click

from gwb_launcher.account import read_accounts_to_monitor_from_json, get_account_with_pid
from gwb_launcher.window import get_gw_client_pids, get_crashed_gw_client_pids


@click.group()
def cli():
    pass


@cli.command("monitor")
@click.option("-a", "--accounts_filename",
              type=click.Path(exists=True))
def monitor(accounts_filename):
    accounts = read_accounts_to_monitor_from_json(accounts_filename)

    for account in accounts:
        account.start()
        account.inject_dll()

    gw_pids_to_watch = list(map(lambda account: account.pid, accounts))
    while True:
        for pid in gw_pids_to_watch:
            existing_gw_client_pids = get_gw_client_pids()
            crashed_gw_client_pids = get_crashed_gw_client_pids()

            if pid not in existing_gw_client_pids:
                account = get_account_with_pid(accounts, pid)
                account.start()
                account.inject_dll()
                gw_pids_to_watch = list(map(lambda account: account.pid, accounts))

            if pid in crashed_gw_client_pids:
                account = get_account_with_pid(accounts, pid)
                account.kill()

        sleep(0.5)
