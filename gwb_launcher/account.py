import json
import subprocess
from pathlib import Path
from time import sleep
from typing import List

import win32api
import win32con

from gwb_launcher.mutex import kill_gw_clients_mutex


class Account:
    def __init__(self, path: str, email: str, password: str, character: str, dll_path: str, use: bool):
        self.path = path
        self.email = email
        self.password = password
        self.character = character
        self.dll_path = dll_path
        self.use = use
        self.pid: int = -1

    def start(self):
        subprocess.Popen([self.path,
                          "-windowed",
                          "-email", self.email,
                          "-password", self.password,
                          "-character", self.character])
        latest_pid = kill_gw_clients_mutex()
        self.pid = int(latest_pid)

        # TODO: Handle character selection screen to be loaded inside dll rather than a long sleep
        sleep(5)

    def inject_dll(self):
        run = subprocess.run(["injector", "-p", f"{self.pid}", "-i", self.dll_path], capture_output=True, text=True)
        print(run.stdout)

    def kill(self):
        handle = win32api.OpenProcess(win32con.PROCESS_TERMINATE, False, self.pid)
        win32api.TerminateProcess(handle, -1)
        win32api.CloseHandle(handle)
        sleep(2.5)


class AccountsDecoder(json.JSONDecoder):
    def decode(self, s, **kwargs):
        obj = super(AccountsDecoder, self).decode(s)
        return [Account(path=account["path"],
                        email=account["email"],
                        password=account["password"],
                        character=account["character"],
                        dll_path=account["dll_path"],
                        use=account["use"])
                for account in obj]


def read_accounts_to_monitor_from_json(account_filename: Path) -> List[Account]:
    with open(account_filename) as account_json_file:
        accounts = json.loads(account_json_file.read(), cls=AccountsDecoder)
        return list(filter(lambda account: account.use, accounts))


def get_account_with_pid(accounts: List[Account], pid: int):
    return list(filter(lambda account: account.pid == pid, accounts))[0]
