import subprocess


def kill_gw_clients_mutex() -> int:
    list_handles_res = subprocess.run(["handle", "-a", "-p", "Gw", "Mutex"], capture_output=True, text=True)

    latest_pid = -1

    for line in list_handles_res.stdout.splitlines():
        str_line = str(line)
        if "AN-Mutex" in str_line:
            pid_loc, pid_length = str_line.find("pid:"), 4
            type_loc, type_length = str_line.find("type: Mutant"), 12

            handle_info = str_line[type_loc + type_length:]
            handle_name_loc = handle_info.find(":")

            pid = str_line[pid_loc + pid_length:type_loc]
            handle_id = handle_info[:handle_name_loc]

            subprocess.run(["handle", "-p", f"{pid}", "-c", f"{handle_id}", "-y"])
            print(f"Mutex with handle id {handle_id} in pid {pid} closed")

            latest_pid = pid

    return int(latest_pid)
